|测试功能|预置条件|输入|预期输出|是否自动|测试结果|
|--------------------------------|--------------------------------|--------------------------------|--------------------------------|--------------------------------|--------------------------------|
|触发绑定到UIAbility前台运行的意图调用|	位于主页| 点击按钮意图绑定到UIAbility前台 | 成功拉起另一个页面，并返回意图调用结果，且与预期结果相同 | 是 |Pass|
|触发绑定到ServiceExtension运行的意图调用|	位于主页| 点击按钮意图绑定到ServiceExtension | 返回意图调用结果且预期结果相同 | 是 |Pass|
